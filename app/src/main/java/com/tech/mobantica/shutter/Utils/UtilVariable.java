package com.tech.mobantica.shutter.Utils;

/**
 * Created by Comp2 on 1/10/2018.
 */

public class UtilVariable {
    public static final String EndPoint = "EndPoint";
    public static final String PoolId = "PoolId";
    public static final String ThingName = "ThingName";

    public static final String ShutterPosition = "ShutterPosition";

    public static final String SSID = "SSID";

    public static final String Password = "Password";

    public static final String WIFIModule = "WIFIModulePassword";

    public static final String ConfigSuccess = "ConfigSuccess";

    //    private String downtopic = "$aws/things/HomAuto_NVir/shadow/update";
//    private String topicCurrentStatus = "$aws/things/HomAuto_NVir/shadow/get";
//    private String uptopic = "$aws/things/HomAuto_NVir/shadow/update";
}


