package com.tech.mobantica.shutter.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;
import com.tech.mobantica.shutter.R;
import com.tech.mobantica.shutter.Utils.DialogUtils;
import com.tech.mobantica.shutter.Utils.Errors;
import com.tech.mobantica.shutter.Utils.UtilMethods;
import com.tech.mobantica.shutter.adapters.HostAdapter;
import com.tech.mobantica.shutter.database.Database;
import com.tech.mobantica.shutter.Utils.AppPrefrences;
import com.tech.mobantica.shutter.pojo.Host;
import com.tech.mobantica.shutter.mSocket.MainAsyncResponse;
import com.tech.mobantica.shutter.mSocket.ScanHostsAsyncTask;
import com.tech.mobantica.shutter.Utils.UserPreference;
import com.tech.mobantica.shutter.mSocket.Wireless;

import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class PortActivity extends AppCompatActivity implements MainAsyncResponse {

    private Wireless wifi;
    private ListView hostList;
    private Button discoverHostsBtn;
    private String discoverHostsStr;
    private Handler signalHandler = new Handler();
    private ProgressDialog progressDialogScanning, progressDialogConfiguration, dialog;
    private Handler scanHandler;
    private IntentFilter intentFilter = new IntentFilter();
    private HostAdapter hostAdapter;
    private List<Host> hosts = Collections.synchronizedList(new ArrayList<Host>());
    private RelativeLayout parentLayout;
    private Database db;
    private ImageView mArrowBack;
    private TextView mTitle;
    private ImageView mManageThings, mSettings, mWifiSettings;
    private Dialog configureConnectionDialog;

    WifiManager wifiManager;

    private AppPrefrences appPrefrences;
    private String mEndPoint = "", mPoolId = "", mThingName = "";

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if (info == null) {
                return;
            }
            getNetworkInfo(info);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_port);
        dialog = new ProgressDialog(PortActivity.this);
        progressDialogConfiguration = new ProgressDialog(PortActivity.this);
        progressDialogConfiguration.setCancelable(false);
        getPreferences();
        initToolBar();
        initWidget();
        initListeners();
        setupHostsAdapter();
        getHostList();
    }

    private void getPreferences() {
        appPrefrences = new AppPrefrences(getApplicationContext());
        mEndPoint = appPrefrences.getEndPoint();
        mThingName = appPrefrences.getThingName();
        mPoolId = appPrefrences.getPoolId();
    }

    private void initListeners() {
        mArrowBack.setVisibility(View.INVISIBLE);
        mTitle.setText("Host");
        mManageThings.setVisibility(View.INVISIBLE);
        mSettings.setVisibility(View.INVISIBLE);
        mWifiSettings.setVisibility(View.INVISIBLE);
    }

    private void initWidget() {
        parentLayout = findViewById(R.id.relativelayout);
        hostList = findViewById(R.id.hostList);
        discoverHostsBtn = findViewById(R.id.btn_discoverHosts);
        discoverHostsStr = getResources().getString(R.string.hostDiscovery);
        discoverHostsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHostList();
            }
        });
        wifi = new Wireless(getApplicationContext());
        scanHandler = new Handler(Looper.getMainLooper());

        db = Database.getInstance(getApplicationContext());
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
    }

    private void initToolBar() {
        mArrowBack = findViewById(R.id.txtBack);
        mTitle = findViewById(R.id.txtTitle);
        mManageThings = findViewById(R.id.txtManageThings);
        mSettings = findViewById(R.id.txtSettings);
        mWifiSettings = findViewById(R.id.manageWifi);
    }

    private void setupHostsAdapter() {
        //To do
        hostAdapter = new HostAdapter(this, hosts);
        hostList.setAdapter(hostAdapter);

        if (!hosts.isEmpty()) {
            discoverHostsBtn.setText(discoverHostsStr + " (" + hosts.size() + ")");
        }
    }

    private void getNetworkInfo(NetworkInfo info) {
        final Resources resources = getResources();
        final Context context = getApplicationContext();

        try {
            boolean enabled = wifi.isEnabled();
            if (!info.isConnected() || !enabled) {
                signalHandler.removeCallbacksAndMessages(null);
                if (progressDialogConfiguration!=null && progressDialogConfiguration.isShowing()) {
                    progressDialogConfiguration.dismiss();
                }
            }

        } catch (Exception e) {
            Errors.showError(context, resources.getString(R.string.failedWifiManager));
            String failedwifiManager = DateFormat.getDateTimeInstance().format(new Date());
            String isEnable = getResources().getString(R.string.failedWifiManager) + " : " + failedwifiManager;
            Errors.saveErrorLogs(this, isEnable);
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, "TAG", "PortActivity");
            FirebaseCrash.report(e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        signalHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onSaveInstanceState(Bundle savedState) {
        super.onSaveInstanceState(savedState);

        ListAdapter adapter = hostList.getAdapter();
        if (adapter != null) {
            ArrayList<Host> adapterData = new ArrayList<>();
            for (int i = 0; i < adapter.getCount(); i++) {
                Host item = (Host) adapter.getItem(i);
                adapterData.add(item);
            }
            savedState.putSerializable("hosts", adapterData);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onRestoreInstanceState(Bundle savedState) {
        super.onRestoreInstanceState(savedState);

        hosts = (ArrayList<Host>) savedState.getSerializable("hosts");
        if (hosts != null) {
            setupHostsAdapter();
        }
    }


    @Override
    public void processFinish(final Host h, final AtomicInteger i) {
        scanHandler.post(new Runnable() {

            @Override
            public void run() {
                hosts.add(h);
                hostAdapter.sort(new Comparator<Host>() {

                    @Override
                    public int compare(Host lhs, Host rhs) {
                        try {
                            int leftIp = ByteBuffer.wrap(InetAddress.getByName(lhs.getIp()).getAddress()).getInt();
                            int rightIp = ByteBuffer.wrap(InetAddress.getByName(rhs.getIp()).getAddress()).getInt();

                            return leftIp - rightIp;
                        } catch (Exception ignored) {
                            return 0;
                        }
                    }
                });
                discoverHostsBtn.setText(discoverHostsStr + " (" + hosts.size() + ")");
                if (i.decrementAndGet() == 0) {
                    discoverHostsBtn.setAlpha(1);
                    discoverHostsBtn.setEnabled(true);
                }
            }
        });
    }


    @Override
    public void processFinish(int output) {
        if (progressDialogScanning != null && progressDialogScanning.isShowing()) {
            progressDialogScanning.incrementProgressBy(output);
        }
    }


    @Override
    public void processFinish(String output) {

    }

    @Override
    public void processFinish(final boolean output) {
        scanHandler.post(new Runnable() {

            @Override
            public void run() {
                hostAdapter.notifyDataSetChanged();
                if (output && progressDialogScanning != null && progressDialogScanning.isShowing()) {
                    progressDialogScanning.dismiss();
                }
            }
        });
    }

    @Override
    public <T extends Throwable> void processFinish(final T output) {
        scanHandler.post(new Runnable() {

            @Override
            public void run() {
                Errors.showError(getApplicationContext(), output.getLocalizedMessage());
            }
        });
    }

    private void getHostList() {
        Resources resources = getResources();
        final Context context = getApplicationContext();
        try {
            if (!wifi.isEnabled()) {
//                Errors.showError(context, resources.getString(R.string.wifiDisabled));
                DialogUtils.showEnableWifiDialog(PortActivity.this);
                String dateTimeEnabled = DateFormat.getDateTimeInstance().format(new Date());
                String isEnable = getResources().getString(R.string.wifiDisabled) + " : " + dateTimeEnabled;
                Errors.saveErrorLogs(this, isEnable);
                return;
            }
            if (!wifi.isConnectedWifi()) {
                //TODO show connect to wifi dialog
//                Errors.showError(context, resources.getString(R.string.notConnectedWifi));
                DialogUtils.showConnectToWifi(PortActivity.this);
                String dateTimeNotConnected = DateFormat.getDateTimeInstance().format(new Date());
                String isEnable = getResources().getString(R.string.notConnectedWifi) + " : " + dateTimeNotConnected;
                Errors.saveErrorLogs(this, isEnable);
                return;
            }
        } catch (Exception e) {
            Errors.showError(context, resources.getString(R.string.failedWifiManager));
            String dateTimeManager = DateFormat.getDateTimeInstance().format(new Date());
            String isEnable = getResources().getString(R.string.failedWifiManager) + " : " + dateTimeManager;
            Errors.saveErrorLogs(this, isEnable);
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, "TAG", "PortActivity");
            FirebaseCrash.report(e);
            return;
        }
        int numSubnetHosts;
        try {
            numSubnetHosts = wifi.getNumberOfHostsInWifiSubnet();
        } catch (Exception e) {
            Errors.showError(context, resources.getString(R.string.failedSubnetHosts));
            String dateTimeSubnet = DateFormat.getDateTimeInstance().format(new Date());
            String isEnable = getResources().getString(R.string.failedSubnetHosts) + " : " + dateTimeSubnet;
            Errors.saveErrorLogs(this, isEnable);
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, "TAG", "PortActivity");
            FirebaseCrash.report(e);
            return;
        }
        hosts.clear();
        discoverHostsBtn.setText(discoverHostsStr);
        progressDialogScanning = new ProgressDialog(PortActivity.this);
        progressDialogScanning.setTitle(resources.getString(R.string.hostScan));
        progressDialogScanning.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialogScanning.setProgress(0);
        progressDialogScanning.setCancelable(false);
        progressDialogScanning.setMax(numSubnetHosts);
        progressDialogScanning.show();

        try {
            Integer ip = wifi.getInternalWifiIpAddress(Integer.class);
            new ScanHostsAsyncTask(PortActivity.this, db).execute(ip, wifi.getInternalWifiSubnet(), UserPreference.getHostSocketTimeout(context));
            discoverHostsBtn.setAlpha(.3f);
            discoverHostsBtn.setEnabled(false);
        } catch (Exception e) {
            Errors.showError(context, resources.getString(R.string.notConnectedWifi));
//            TODO show enable wifi dialog
            String currentDateTimeStringEnable = DateFormat.getDateTimeInstance().format(new Date());
            String isEnable = getResources().getString(R.string.notConnectedWifi) + " : " + currentDateTimeStringEnable;
            Errors.saveErrorLogs(this, isEnable);

            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, "TAG", "PortActivity");
            FirebaseCrash.report(e);

        }

        hostList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String ipValue = hosts.get(i).getIp().toString();
                socketConnection(ipValue);
            }
        });

    }

    private void socketConnection(final String ipValue) {
        configureConnectionDialog = new Dialog(PortActivity.this);
        configureConnectionDialog.setContentView(R.layout.socket);
        configureConnectionDialog.setTitle("Network Information");
        final EditText etSSID = configureConnectionDialog.findViewById(R.id.ssid);
        final EditText etPASSWORD = configureConnectionDialog.findViewById(R.id.password);
        Button btnCancel = configureConnectionDialog.findViewById(R.id.btnNigative);
        Button btnConnect = configureConnectionDialog.findViewById(R.id.btnPositive);
        CheckBox checkBox_showPassword = configureConnectionDialog.findViewById(R.id.checkBox_showPassword);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ssid = etSSID.getText().toString();
                String password = etPASSWORD.getText().toString();
                if (ssid.isEmpty() || ssid.length() == 0 || ssid.equals("") || ssid == null) {
                    etSSID.setError("Invalid");
                    return;
                } else if (password.isEmpty() || password.length() == 0 || password.equals("") || password == null) {
                    etPASSWORD.setError("Invalid");
                    return;
                } else {
                    UtilMethods.hideSoftKeyBoard(PortActivity.this);
                    progressDialogConfiguration.setMessage("Configure...");
                    progressDialogConfiguration.show();
                    String connectionString = "\\cfg\\SSID:\"" + ssid + "\"\\PSWD:\"" + password + "\"\\THNM:\"palmat2\"";
                    new connectSocketTask().execute(connectionString, ipValue, ssid, password);
                    configureConnectionDialog.dismiss();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                configureConnectionDialog.dismiss();
            }
        });

        checkBox_showPassword.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    etPASSWORD.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else {
                    etPASSWORD.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.copyFrom(dialog.getWindow().getAttributes());
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        configureConnectionDialog.show();
        configureConnectionDialog.getWindow().setAttributes(params);

    }

    public class connectSocketTask extends AsyncTask<String, Void, String> {

        private String mString = "";
        private String readMessage;

        @Override
        protected String doInBackground(final String... connectionString) {

            Socket pingSocket = null;
            PrintWriter out = null;
            try {
                pingSocket = new Socket(connectionString[1], 5045); // ip value
                out = new PrintWriter(pingSocket.getOutputStream(), true);
                mString = connectionString[0];   // ip connection string
                out.println(mString);    // writing string on ip

                byte[] buffer = new byte[2048];
                int bytes;
                InputStream ing = pingSocket.getInputStream();
                bytes = ing.read(buffer);
                readMessage = new String(buffer, 0, bytes);
                Log.d("MainActivity->", "Message :: " + readMessage);

                if (readMessage.equals("\\cfg\\ACK")) {
                    progressDialogConfiguration.dismiss();
                    saveSuccessString("\\cfg\\ACK");
                    if ((mEndPoint.equals("") && mPoolId.equals("")) || mThingName.equals("")) {
                        startActivity(new Intent(PortActivity.this, ManageDeviceActivity.class));
                    } else {
                        AppPrefrences myPrefs = new AppPrefrences(PortActivity.this);
                        myPrefs.setConfiguredFlag(true);
                        startActivity(new Intent(PortActivity.this, ShutterActivity.class));
                    }

                } else {  //if(readMessage.equals("\\cfg\\NAK"))
                    //ToDo Chane message
                    mString = "Try again.\nNetwork connection error...!";
                }

            } catch (Exception e) {
//                mString = e.getMessage();
                mString = "Try again.\nNetwork connection error...!";
                e.printStackTrace();
                FirebaseCrash.logcat(Log.ERROR, "TAG", "PortActivity");
                FirebaseCrash.report(e);

            } finally {
                if (null != out) {
                    out.close();
                }
                try {
                    if (null != pingSocket) {
                        pingSocket.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    FirebaseCrash.logcat(Log.ERROR, "TAG", "PortActivity");
                    FirebaseCrash.report(e);
                    return e.getMessage();
                }
            }
            return mString;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Snackbar snackbar = Snackbar
                    .make(parentLayout, result, Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // connectToMqtt();

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                }
                            }, 10000);
                        }
                    });
            progressDialogConfiguration.dismiss();
            snackbar.show();
            String currentDateTimeStringEnable = DateFormat.getDateTimeInstance().format(new Date());
            String response = result + " : " + currentDateTimeStringEnable;
            Errors.saveErrorLogs(PortActivity.this, response);
        }

        private void saveSSIDPASS(String ssid, String password) {
            AppPrefrences appPrefrences = new AppPrefrences(PortActivity.this);
            appPrefrences.setSSIDAndPassword(ssid, password);
        }

        private void saveSuccessString(String s) {
            AppPrefrences appPrefrences = new AppPrefrences(PortActivity.this);
            appPrefrences.setSuccessString(s);
        }
    }

    private class asyncDataSetChanged extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Loading");
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            hostAdapter.notifyDataSetChanged();
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
        }
    }
}


