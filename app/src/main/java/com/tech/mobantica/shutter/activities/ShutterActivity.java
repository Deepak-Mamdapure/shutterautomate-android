package com.tech.mobantica.shutter.activities;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttMessageDeliveryCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;
import com.amazonaws.regions.Regions;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.crash.FirebaseCrash;
import com.tech.mobantica.shutter.MainActivity;
import com.tech.mobantica.shutter.R;
import com.tech.mobantica.shutter.Utils.AppPrefrences;
import com.tech.mobantica.shutter.Utils.ConnectionReceiver;
import com.tech.mobantica.shutter.Utils.DialogUtils;
import com.tech.mobantica.shutter.Utils.Errors;
import com.tech.mobantica.shutter.Utils.PageNavigationStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.tech.mobantica.shutter.Utils.UtilMethods.isNetworkAvailable;

public class ShutterActivity extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener, View.OnLongClickListener, ConnectionReceiver.ConnectionReceiverListener {

    private static final String TAG = "ShutterActivity";
    static final String LOG_TAG = MainActivity.class.getCanonicalName();
    private String mEndPoint = "", mPoolId = "", mThingName = "";
    private ImageButton imgBtn_up, imgBtn_down, imgBtn_stop, imgBtn_heart;

    private String urlJsonObj = "https://api.androidhive.info/volley/person_object.json";

    private String BASE_TOPIC = "";
    // Region of AWS IoT
    private static final Regions MY_REGION = Regions.US_EAST_1;
    private String downtopic, topicCurrentStatus, uptopic;
    private String clientId, uiStatus, stopmessage;

    long lastDown, lastDuration;
    AWSIotMqttManager mqttManager;
    public boolean status = true;
    private ImageView mShutterGate;
    private ProgressBar progressBar;
    AWSCredentials awsCredentials;

    CognitoCachingCredentialsProvider credentialsProvider;

    private List<Animator> mAnimatorList = new ArrayList<>();
    private String upmessage = "", downmessage = "";

    // private long upTime = 8000,downTime = 10000;
    private long upTime, downTime;
    private TextView txtTitle, txtStatus;

    private RelativeLayout parentLayout;

    private SharedPreferences preferences;

    private AppPrefrences appPrefrences;
    private ImageView txtBack, txtSettings, txtManageDevice, mWifiSetting;

    //End point - azur83gz1t6vj.iot.us-east-1.amazonaws.com
    //Pool ID - us-east-1:e9d8b374-4a40-48d2-b32d-cf8f52c87fbb
    //Policy name - palmatAPP_policy
    // thing name - HomAuto_NVir

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shutter);
        upmessage = "{\"state\":{\"desired\":{\"D8\":false},\"reported\":{\"D6\":true,\"D7\":false}}}";
        downmessage = "{\"state\":{\"desired\":{\"D8\":false},\"reported\":{\"D6\":false,\"D7\":true}}}";
        stopmessage = "{\"state\":{\"desired\":{\"D8\":false},\"reported\":{\"D6\":false,\"D7\":false}}}";
        mShutterGate = (ImageView) findViewById(R.id.shuttergate);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        getDeviceValueFromSharedPreferences();
        initToolbar();
        initWidget();
        initListeners();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isNetworkAvailable(ShutterActivity.this)) {
            setTopics();
            initCognitoCachingCredentialsProvider();
            clientId = UUID.randomUUID().toString();
            connectToMqtt();
            setUpDownTime();

        } else {
            DialogUtils.showEnableWifiDialog(ShutterActivity.this);
            progressBar.setVisibility(View.GONE);
        }
    }

    private void setTopics() {
        BASE_TOPIC = "$aws/things/" + mThingName + "/shadow/";
        downtopic = BASE_TOPIC + "update";
        topicCurrentStatus = BASE_TOPIC + "get";
        uptopic = BASE_TOPIC + "update";
    }


    private void initCognitoCachingCredentialsProvider() {
        // Initialize the AWS Cognito credentials provider

        if (!mPoolId.equals("")) {
            credentialsProvider = new CognitoCachingCredentialsProvider(
                    getApplicationContext(), // context
                    mPoolId, // Identity Pool ID
                    MY_REGION // Region
            );
        }
    }

    private void showSnackbar() {
        Snackbar snackbar = Snackbar
                .make(parentLayout, "Please check your internet connection and retry", Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        connectToMqtt();
                    }
                });

        snackbar.show();
    }

    private void connectToMqtt() {
        if (isNetworkAvailable(ShutterActivity.this)) {

            if (mEndPoint.equals("")) {
                Log.d(TAG, "connectToMqtt: " + "No broker");
                startActivity(new Intent(ShutterActivity.this, ManageDeviceActivity.class));
            } else {
                Log.d(TAG, "connectToMqtt: " + "Available broker");
                try {
                    progressBar.setVisibility(View.VISIBLE);
                    mqttManager = new AWSIotMqttManager(clientId, mEndPoint);
                    new ConnectToMQTT().execute();
//
                } catch (Exception e) {
                    Log.d(TAG, "connectToMqtt: Exception");
                    e.printStackTrace();
                    isInternetConnected();
                    String currentDate = DateFormat.getDateTimeInstance().format(new Date());
                    String isEnable = e.getMessage() + " : " + currentDate;
                    Errors.saveErrorLogs(this, isEnable);
                    e.printStackTrace();
                }
            }

        } else {
            showSnackbar();
        }

    }

    private void initListeners() {

//        imgBtn_heart.setOnClickListener(clickListener_imgBtnHeart);
        imgBtn_down.setOnTouchListener(this);
        imgBtn_down.setOnLongClickListener(this);
        imgBtn_up.setOnTouchListener(this);
        imgBtn_up.setOnLongClickListener(this);
        imgBtn_stop.setOnTouchListener(this);
    }

    private void initWidget() {
        imgBtn_up = (ImageButton) findViewById(R.id.up);
        imgBtn_down = (ImageButton) findViewById(R.id.down);
        imgBtn_stop = (ImageButton) findViewById(R.id.stop);

        txtStatus = (TextView) findViewById(R.id.txtStatus);
        imgBtn_heart = (ImageButton) findViewById(R.id.btn_heart);
        parentLayout = (RelativeLayout) findViewById(R.id.parentLayoutd);

    }

    private void initToolbar() {
        txtBack = (ImageView) findViewById(R.id.txtBack);
        txtBack.setVisibility(View.INVISIBLE);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText("PALMAT");
        txtSettings = (ImageView) findViewById(R.id.txtSettings);
        txtSettings.setOnClickListener(this);
        txtManageDevice = (ImageView) findViewById(R.id.txtManageThings);
        txtManageDevice.setOnClickListener(this);
        mWifiSetting = (ImageView) findViewById(R.id.manageWifi);
        mWifiSetting.setOnClickListener(this);
    }

    private void setUpDownTime() {
        upTime = appPrefrences.getUpTime();
        downTime = appPrefrences.getDownTime();
    }

    private void getDeviceValueFromSharedPreferences() {
        appPrefrences = new AppPrefrences(getApplicationContext());
        mEndPoint = appPrefrences.getEndPoint();
        mThingName = appPrefrences.getThingName();
        mPoolId = appPrefrences.getPoolId();

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    void updateUiScreenFirstTime() {

        if (uiStatus != "") {
            if (uiStatus.equals("true")) {
                progressBar.setVisibility(View.GONE);
                mShutterGate.setVisibility(View.VISIBLE);
                Animator animator = AnimatorInflater.loadAnimator(ShutterActivity.this, R.animator.object_animator_up);
                animator.setTarget(mShutterGate);
                long duration = appPrefrences.getLastKnownDuration();
                Log.d(TAG, "updateUiScreenFirstTime: loaction "+duration);
                animator.setDuration(duration);
                ObjectAnimator objectAnimator = (ObjectAnimator) animator;
                objectAnimator.ofFloat(mShutterGate,"translationX",duration);
                mAnimatorList.add(animator);
            }
            updateUiScreen();
        } else {
            progressBar.setVisibility(View.VISIBLE);
            txtStatus.setText("Connecting...");
        }

    }

    void updateUiScreen() {

        Log.d(TAG, "updateUiScreen: update ui screeen called");
        if (uiStatus != "") {
            progressBar.setVisibility(View.GONE);
            if (uiStatus.equals("false")) {
                mShutterGate.setVisibility(View.VISIBLE);
                Animator animator = AnimatorInflater.loadAnimator(ShutterActivity.this, R.animator.object_animator_down);
                animator.setTarget(mShutterGate);
                //long duration = 10000;
                animator.setDuration(downTime);
                animator.start();
                mAnimatorList.add(animator);
            } else {
                mShutterGate.setVisibility(View.VISIBLE);
                Animator animator = AnimatorInflater.loadAnimator(ShutterActivity.this, R.animator.object_animator_up);
                animator.setTarget(mShutterGate);
                //long duration = 8000;
                animator.setDuration(upTime);
                animator.start();
                mAnimatorList.add(animator);
            }
        } else {
            progressBar.setVisibility(View.VISIBLE);
        }
    }


    void connect() {

        try {
            mqttManager.connect(credentialsProvider, new AWSIotMqttClientStatusCallback() {
                @Override
                public void onStatusChanged(final AWSIotMqttClientStatus status,
                                            final Throwable throwable) {
                    Log.d(LOG_TAG, "Status = " + String.valueOf(status));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (status == AWSIotMqttClientStatus.Connecting) {
                                // tvStatus.setText("Connecting...");
                                //Toast.makeText(ActivityScanWifi.this, "Connecting...", Toast.LENGTH_SHORT).show();

                                txtStatus.setText("Connecting.....");

                            } else if (status == AWSIotMqttClientStatus.Connected) {
                                // tvStatus.setText("Connected");
                                // Toast.makeText(ActivityScanWifi.this, "Connected", Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                subscribeTopics();
                                getCurrentStatus();

                                txtStatus.setText("Connected & Listening...");


                            } else if (status == AWSIotMqttClientStatus.Reconnecting) {
                                if (throwable != null) {
                                    progressBar.setVisibility(View.GONE);
                                    Log.e(LOG_TAG, "Connection error.", throwable);
                                }

                            } else if (status == AWSIotMqttClientStatus.ConnectionLost) {
                                if (throwable != null) {
                                    progressBar.setVisibility(View.GONE);
                                    Log.e(LOG_TAG, "Connection error.", throwable);
                                    throwable.printStackTrace();
                                }

                            } else {
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            });
        } catch (Exception e) {
            isInternetConnected();
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        switch (view.getId()) {
            case R.id.up:
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                    try {
                        mqttManager.publishString(stopmessage, downtopic, AWSIotMqttQos.QOS0, new AWSIotMqttMessageDeliveryCallback() {
                            @Override
                            public void statusChanged(MessageDeliveryStatus status, Object userData) {
                                Log.e(LOG_TAG, "Release Event Stop");
                            }
                        }, status);
                    } catch (Exception e) {
                        isInternetConnected();
                        e.printStackTrace();
                    }
                    for (Animator animator : mAnimatorList) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            animator.pause();
                            if (appPrefrences != null) {
                                ObjectAnimator objectAnimator = (ObjectAnimator) animator;
                                objectAnimator.getAnimatedFraction();
                                Log.d(TAG, "onClick: last location "+animator.getDuration());
                                appPrefrences.setLastKnownDuration(animator.getDuration());
                            }
                        }
                    }
                }

                break;

            case R.id.down:
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        mqttManager.publishString(stopmessage, downtopic, AWSIotMqttQos.QOS0, new AWSIotMqttMessageDeliveryCallback() {
                            @Override
                            public void statusChanged(MessageDeliveryStatus status, Object userData) {
                                Log.e(LOG_TAG, "Release Event Stop");
                            }
                        }, status);
                    } catch (Exception e) {
                        isInternetConnected();
                        e.printStackTrace();
                    }

                    for (Animator animator : mAnimatorList) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            animator.pause();
                            if (appPrefrences != null) {
                                appPrefrences.setLastKnownDuration(animator.getDuration());
                            }
                        }
                    }

                }
                break;

            case R.id.stop:

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    for (Animator animator : mAnimatorList) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            animator.pause();
                            if (appPrefrences != null) {
                                appPrefrences.setLastKnownDuration(animator.getDuration());
                            }
                        }
                    }
                    //publish**************************************************************************************************
                    try {
                        mqttManager.publishString(stopmessage, downtopic, AWSIotMqttQos.QOS0, new AWSIotMqttMessageDeliveryCallback() {
                            @Override
                            public void statusChanged(MessageDeliveryStatus status, Object userData) {
                                Log.e(LOG_TAG, "Button Stop");
                                //Toast.makeText(ActivityScanWifi.this, "Status Down: "+status, Toast.LENGTH_SHORT).show();
                                //Toast.makeText(ActivityScanWifi.this, "Success", Toast.LENGTH_SHORT).show();
                            }
                        }, status);
                    } catch (Exception e) {
                        isInternetConnected();
                        e.printStackTrace();
                    }
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                }

                break;
        }

        return false;
    }


    View.OnClickListener btnStop = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

//            imgBtn_stop.setEnabled(false);
//            imgBtn_up.setEnabled(true);
//            imgBtn_down.setEnabled(true);

            for (Animator animator : mAnimatorList) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    animator.pause();
                    if (appPrefrences != null) {
                        Log.d(TAG, "onClick: last location "+animator.getDuration());
                        appPrefrences.setLastKnownDuration(animator.getDuration());
                    }
                }
            }

            //publish**************************************************************************************************

            try {
                mqttManager.publishString(stopmessage, downtopic, AWSIotMqttQos.QOS0, new AWSIotMqttMessageDeliveryCallback() {
                    @Override
                    public void statusChanged(MessageDeliveryStatus status, Object userData) {
                        Log.e(LOG_TAG, "Button Stop");
                        //Toast.makeText(ActivityScanWifi.this, "Status Down: "+status, Toast.LENGTH_SHORT).show();
                        //Toast.makeText(ActivityScanWifi.this, "Success", Toast.LENGTH_SHORT).show();
                    }
                }, status);
            } catch (Exception e) {
                isInternetConnected();
                e.printStackTrace();
            }

        }
    };

    void getCurrentStatus() {

        try {
            mqttManager.publishString("", topicCurrentStatus, AWSIotMqttQos.QOS0, new AWSIotMqttMessageDeliveryCallback() {
                @Override
                public void statusChanged(MessageDeliveryStatus status, Object userData) {
                    Log.e(LOG_TAG, "Current Status...");
                    //Toast.makeText(ActivityScanWifi.this, "Status Up: "+status, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(ActivityScanWifi.this, "Success", Toast.LENGTH_SHORT).show();
                }
            }, status);
        } catch (Exception e) {
            isInternetConnected();
            e.printStackTrace();
        }

    }

    //click listener for imgBtn_heart
    View.OnClickListener btnL = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Toast.makeText(ShutterActivity.this, "None", Toast.LENGTH_SHORT).show();
        }
    };

    void subscribeTopics() {
        try {
            mqttManager.subscribeToTopic(BASE_TOPIC + "get/accepted", AWSIotMqttQos.QOS0, new AWSIotMqttNewMessageCallback() {
                @Override
                public void onMessageArrived(final String topic, final byte[] data) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String message = new String(data, "UTF-8");
                                Log.d(LOG_TAG, "Message arrived:");
                                Log.d(LOG_TAG, "   Topic: " + topic);
                                Log.d(LOG_TAG, " Message get accepted: " + message);


                                JSONObject jsonObject = new JSONObject(message);
                                JSONObject jsonState = jsonObject.getJSONObject("state");
                                JSONObject jsonDesired = jsonState.getJSONObject("reported");
                                uiStatus = jsonDesired.getString("D6");

                                Log.d(LOG_TAG, "JsonResponse: " + String.valueOf(uiStatus));
                                progressBar.setVisibility(View.VISIBLE);
                                updateUiScreenFirstTime();

                            } catch (UnsupportedEncodingException e) {
                                Log.e(LOG_TAG, "Message encoding error.", e);
                                progressBar.setVisibility(View.GONE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            });
        } catch (Exception e) {
            Log.e(LOG_TAG, "Subscription error.", e);
            isInternetConnected();
            progressBar.setVisibility(View.GONE);
        }

        try {
            mqttManager.subscribeToTopic(BASE_TOPIC + "get/rejected", AWSIotMqttQos.QOS0, new AWSIotMqttNewMessageCallback() {
                @Override
                public void onMessageArrived(final String topic, final byte[] data) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String message = new String(data, "UTF-8");
                                Log.d(LOG_TAG, "Message arrived:");
                                Log.d(LOG_TAG, "   Topic: " + topic);
                                Log.d(LOG_TAG, " Message get rejected: " + message);

                                //tvLastMessage.setText(message);

                            } catch (UnsupportedEncodingException e) {
                                Log.e(LOG_TAG, "Message encoding error.", e);
                            }
                        }
                    });
                }
            });
        } catch (Exception e) {
            Log.e(LOG_TAG, "Subscription error.", e);
            isInternetConnected();
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, "TAG", "ShutterActivity");
            FirebaseCrash.report(e);
        }

        try {
            mqttManager.subscribeToTopic(BASE_TOPIC + "update/accepted", AWSIotMqttQos.QOS0, new AWSIotMqttNewMessageCallback() {
                @Override
                public void onMessageArrived(final String topic, final byte[] data) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String message = new String(data, "UTF-8");
//                                Log.d(LOG_TAG, "Message arrived:");
//                                Log.d(LOG_TAG, "   Topic: " + topic);
                                Log.d(LOG_TAG, " Message update accepted: " + message);
                                //tvLastMessage.setText(message);
                                JSONObject jsonObject = new JSONObject(message);
                                JSONObject jsonState = jsonObject.getJSONObject("state");
                                JSONObject jsonDesired = jsonState.getJSONObject("reported");
                                uiStatus = jsonDesired.getString("D6");

                                if (jsonDesired.getString("D6").equals("true") && jsonDesired.getString("D7").equals("false")) {
                                    updateUiScreen();
                                }

                                if (jsonDesired.getString("D6").equals("false") && jsonDesired.getString("D7").equals("true")) {
                                    updateUiScreen();
                                }

                                if (jsonDesired.getString("D6").equals("false") && jsonDesired.getString("D7").equals("false")) {
//                                    stopUiScreen();
                                    return;
                                }

                                Log.d(LOG_TAG, "JsonResponse: " + String.valueOf(uiStatus));

                            } catch (UnsupportedEncodingException e) {
                                Log.e(LOG_TAG, "Message encoding error.", e);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        } catch (Exception e) {
            Log.e(LOG_TAG, "Subscription error.", e);
            isInternetConnected();
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, "TAG", "ShutterActivity");
            FirebaseCrash.report(e);
        }
        try {
            mqttManager.subscribeToTopic(BASE_TOPIC + "update/rejected", AWSIotMqttQos.QOS0, new AWSIotMqttNewMessageCallback() {
                @Override
                public void onMessageArrived(final String topic, final byte[] data) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String message = new String(data, "UTF-8");
                                Log.d(LOG_TAG, "Message arrived:");
                                Log.d(LOG_TAG, "   Topic: " + topic);
                                Log.d(LOG_TAG, " Message update rejected: " + message);


                            } catch (UnsupportedEncodingException e) {
                                Log.e(LOG_TAG, "Message encoding error.", e);
                                e.printStackTrace();
                                FirebaseCrash.logcat(Log.ERROR, "TAG", "ShutterActivity");
                                FirebaseCrash.report(e);
                            }
                        }
                    });
                }
            });
        } catch (Exception e) {
            Log.e(LOG_TAG, "Subscription error.", e);
            isInternetConnected();
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, "TAG", "ShutterActivity");
            FirebaseCrash.report(e);
        }
    }

    private void stopUiScreen() {
        try {
            mqttManager.publishString(stopmessage, downtopic, AWSIotMqttQos.QOS0, new AWSIotMqttMessageDeliveryCallback() {
                @Override
                public void statusChanged(MessageDeliveryStatus status, Object userData) {
                    Log.e(LOG_TAG, "Release Event Stop");
                    //Toast.makeText(ActivityScanWifi.this, "Status Down: "+status, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(ActivityScanWifi.this, "Success", Toast.LENGTH_SHORT).show();
                }
            }, status);
        } catch (Exception e) {
            e.printStackTrace();
            isInternetConnected();
        }

        for (Animator animator : mAnimatorList) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                animator.pause();
                if (appPrefrences != null) {
                    appPrefrences.setLastKnownDuration(animator.getDuration());
                }
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.txtSettings:
                startActivity(new Intent(ShutterActivity.this, ActivitySettingNew.class));
                break;

            case R.id.txtManageThings:
                startActivity(new Intent(ShutterActivity.this, ManageDeviceActivity.class));
                break;

            case R.id.manageWifi:
                PageNavigationStatus.isFromShutterActivity = true;
                startActivity(new Intent(ShutterActivity.this, MainActivity.class));
        }
    }

    @Override
    public boolean onLongClick(View v) {

        switch (v.getId()) {
            case R.id.up:
                try {
                    mqttManager.publishString(upmessage, uptopic, AWSIotMqttQos.QOS0, new AWSIotMqttMessageDeliveryCallback() {
                        @Override
                        public void statusChanged(MessageDeliveryStatus status, Object userData) {
                            Log.e(LOG_TAG, "Press Event Up" + "status " + status);
                            //Toast.makeText(ActivityScanWifi.this, "Status Up: "+status, Toast.LENGTH_SHORT).show();
                        }
                    }, status);
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    isInternetConnected();
                }
            case R.id.down:
                try {
                    mqttManager.publishString(downmessage, downtopic, AWSIotMqttQos.QOS0, new AWSIotMqttMessageDeliveryCallback() {
                        @Override
                        public void statusChanged(MessageDeliveryStatus status, Object userData) {
                            Log.e(LOG_TAG, "Press Event Down");
                            //Toast.makeText(ActivityScanWifi.this, "Status Down: "+status, Toast.LENGTH_SHORT).show();
                            //Toast.makeText(ActivityScanWifi.this, "Success", Toast.LENGTH_SHORT).show();
                        }
                    }, status);
                } catch (Exception e) {
                    Log.d(TAG, "onTouch: exc = " + e.getMessage());
                    e.printStackTrace();
                    isInternetConnected();
                }
                break;
        }
        return false;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        isInternetConnected();
    }

    public class ConnectToMQTT extends AsyncTask<Void, Void, AWSCredentials> {

        @Override
        protected AWSCredentials doInBackground(Void... voids) {
            try {
                return credentialsProvider.getCredentials();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(AWSCredentials s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);
            if (null == s) {
                Toast.makeText(ShutterActivity.this, "Check internet connection ...", Toast.LENGTH_LONG).show();
            } else {
                awsCredentials = s;
            }
            makePindTest();
        }
    }

    private void makePindTest() {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlJsonObj, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                connect();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                DialogUtils.showOfflineDialog(ShutterActivity.this);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void isInternetConnected() {
        if (isNetworkAvailable(ShutterActivity.this)) {
            makePindTest();
        } else {
            DialogUtils.showConnectToWifi(ShutterActivity.this);
        }
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {

        }
    }
}
