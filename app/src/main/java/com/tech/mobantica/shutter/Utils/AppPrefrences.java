package com.tech.mobantica.shutter.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import static com.tech.mobantica.shutter.Utils.UtilVariable.ConfigSuccess;
import static com.tech.mobantica.shutter.Utils.UtilVariable.EndPoint;
import static com.tech.mobantica.shutter.Utils.UtilVariable.Password;
import static com.tech.mobantica.shutter.Utils.UtilVariable.PoolId;
import static com.tech.mobantica.shutter.Utils.UtilVariable.SSID;
import static com.tech.mobantica.shutter.Utils.UtilVariable.ShutterPosition;
import static com.tech.mobantica.shutter.Utils.UtilVariable.ThingName;

/**
 * Created by Comp2 on 1/5/2018.
 */

public class AppPrefrences {

    private Context ctx;
    private String prefName = "AppPrefs";
    private String upTime = "UpTime";
    private String downTime = "DownTime";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public AppPrefrences(Context context) {
        this.ctx = context;
        if (preferences == null) {
            preferences = ctx.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        }
        if (editor == null) {
            editor = preferences.edit();
        }
    }

    public void setUpDownTime(long up, long down) {
//        editor.clear();
        editor.putLong(upTime, up);
        editor.putLong(downTime, down);
        editor.commit();
    }

    public long getUpTime() {
        return preferences.getLong(upTime, 8000);
    }

    public long getDownTime() {
        return preferences.getLong(downTime, 10000);
    }

    //Setting Device Value**********************************************************************************************

    public void setDeviceManageValue(String endPoint, String poolId, String thingName) {
//        editor.clear();
        editor.putString(EndPoint, endPoint);
        editor.putString(PoolId, poolId);
        editor.putString(ThingName, thingName);
        editor.commit();
    }

    public String getEndPoint() {
        return preferences.getString(EndPoint, "azur83gz1t6vj.iot.us-east-1.amazonaws.com");  //azur83gz1t6vj.iot.us-east-1.amazonaws.com
    }

    public String getPoolId() {
        return preferences.getString(PoolId, "us-east-1:e9d8b374-4a40-48d2-b32d-cf8f52c87fbb");  // us-east-1:e9d8b374-4a40-48d2-b32d-cf8f52c87fbb
    }

    public String getThingName() {
        return preferences.getString(ThingName, "palmat2"); //
    }

    //shutter position**********************************************************************************************

    public void setShutterCurrentPosition(long position) {
//        editor.clear();
        editor.putLong(ShutterPosition, position);
        editor.commit();
    }

    public String getShutterPosition() {
        return preferences.getString(ShutterPosition, "");
    }

    //SSID and Password**********************************************************************************************

    public void setSSIDAndPassword(String ssid,String password){
        editor.putString(SSID,ssid);
        editor.putString(Password,password);
        editor.commit();
    }

    public String getSSID(){
        return preferences.getString(SSID,"");
    }

    public String getPassword(){
        return preferences.getString(Password,"");
    }

    //Clear Preferences**********************************************************************************************

    public void clearSSIDAndPass(){
        editor.remove(SSID);
        editor.remove(Password);
        editor.commit();
    }

    /*public void clearSuccessString(){
        editor.remove(ConfigSuccess);
        editor.commit();
    }*/

    //Save Success String**********************************************************************************************

    public void setSuccessString(String successConfigurationStatus){
        editor.putString(ConfigSuccess,successConfigurationStatus);
        editor.commit();
    }

    public String getSuccessString(){
        return preferences.getString(ConfigSuccess,"");
    }

    public void setConfiguredFlag(boolean isConfigured){
           editor.putBoolean("isConfigured",isConfigured);
           editor.commit();
    }

    public boolean getConfiguredFlag(){
        return preferences.getBoolean("isConfigured",false);
    }

    public void setLastKnownDuration(long duration){
        editor.putLong("lastKnownDuration",duration);
        editor.commit();
    }

    public long getLastKnownDuration(){
        return preferences.getLong("lastKnownDuration",0);
    }
}