package com.tech.mobantica.shutter.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;

import com.tech.mobantica.shutter.R;
import com.tech.mobantica.shutter.activities.ShutterActivity;

/**
 * Created by Comp2 on 2/6/2018.
 */

public class DialogUtils {


    //alert dialog for setting wifi and redirect user to wifi settings
    public static void showEnableWifiDialog(final Activity _context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setMessage(R.string.str_enable_wifi)
                .setPositiveButton(R.string.str_enable, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK);
                        _context.startActivity(intent);
                    }
                });
        builder.create().show();
    }

    //alert dialog for enable location and redirect user to location settings
    public static void showEnableLocationDialog(final Activity _context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setMessage(R.string.str_enable_loaction)
                .setPositiveButton(R.string.str_enable, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        _context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        builder.create().show();
    }

    public static void showConnectToWifi(final Activity _context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setMessage(R.string.str_pleaseConnectToWifi)
                .setPositiveButton(R.string.str_enable, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        _context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                });
        builder.create().show();

    }

    public static void showOfflineDialog(final Activity _context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle(R.string.str_offline)
                .setMessage(R.string.str_pleaseConnectToLiveNetwork)
                .setPositiveButton(R.string.str_connect, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        _context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                }).create().show();
    }

}
